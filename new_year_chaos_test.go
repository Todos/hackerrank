package main

import (
	"testing"
)

func check(t *testing.T, q []int32, exp int) {
	result := minimumBribesResultInt(q)
	if result != exp {
		t.Error("For", q,
			"expected", exp,
			"got", result)
	}
}

func Test000(t *testing.T) {
	check(t, []int32{2, 1, 5, 3, 4}, 3)
}

func Test001(t *testing.T) {
	check(t, []int32{2, 5, 1, 3, 4}, -1)
}

func Test010(t *testing.T) {
	check(t, []int32{5, 1, 2, 3, 7, 8, 6, 4}, -1)
}

func Test011(t *testing.T) {
	check(t, []int32{1, 2, 5, 3, 7, 8, 6, 4}, 7)
}

func Test110(t *testing.T) {
	check(t, []int32{1, 2, 5, 3, 4, 7, 8, 6}, 4)
}
