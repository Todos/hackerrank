package main

import (
	"sort"
	"testing"
)

func equalSlices(a1 []int64, a2 []int64) bool {
	if (a1 == nil) != (a2 == nil) {
		return false
	}

	if len(a1) != len(a2) {
		return false
	}

	sort.Slice(a1, func(i, j int) bool { return a1[i] < a1[j] })
	sort.Slice(a2, func(i, j int) bool { return a2[i] < a2[j] })

	for i, x := range a1 {
		if x != a2[i] {
			return false
		}
	}
	return true
}

func check(t *testing.T, result []int64, expected [][]int64, elements_check ...bool) {
	if len(elements_check) > 0 && elements_check[0] == true {
		var found bool = false
		for _, ex := range expected {
			if equalSlices(result, ex) {
				found = true
			}
		}

		if !found {
			t.Error("For", expected,
				"Got", result)
		}
	} else {
		var sumRes int64 = 0
		for _, r := range result {
			sumRes += r
		}
		var sumExp int64 = 0
		for _, e := range expected[0] {
			sumExp += e
		}
		if sumRes != sumExp {
			t.Error("For", sumExp,
				"Got", sumRes)

		}
	}
}

func cc(t *testing.T, args []int64, expected [][]int64) {
	result := bonetrousle(int64(args[0]), int64(args[1]), int32(args[2]))
	check(t, result, expected)
}

func Test01(t *testing.T) {
	if !equalSlices(nil, nil) {
		t.Error("nil should equal nil")
	}

	if !equalSlices([]int64{}, []int64{}) {
		t.Fail()
	}

	if equalSlices([]int64{12, 13}, []int64{11}) {
		t.Fail()
	}

	if !equalSlices([]int64{12, 13}, []int64{12, 13}) {
		t.Fail()
	}
}

func Test1(t *testing.T) {
	result := bonetrousle(12, 8, 3)
	var expected [][]int64 = [][]int64{{1, 3, 8}, {2, 3, 7}, {1, 4, 7}}
	check(t, result, expected)
}

func Test2(t *testing.T) {
	result := bonetrousle(10, 3, 3)
	var expected []int64 = []int64{-1}
	if !equalSlices(result, expected) {
		t.Error("For", expected,
			"Got", result)
	}
}

func Test3(t *testing.T) {
	result := bonetrousle(9, 10, 2)
	var expected [][]int64 = [][]int64{{5, 4}, {1, 8}}
	check(t, result, expected)
}

func Test4(t *testing.T) {
	result := bonetrousle(22, 7, 6)
	var expected [][]int64 = [][]int64{{7, 1, 2, 3, 4, 5}}
	check(t, result, expected)
}

func Test5(t *testing.T) {
	result := bonetrousle(26, 7, 6)
	var expected [][]int64 = [][]int64{{3, 1, 4, 5, 6, 7}}
	check(t, result, expected)
}

func Test6(t *testing.T) {
	result := bonetrousle(25, 10, 5)
	var expected [][]int64 = [][]int64{{9, 1, 2, 3, 10}}
	check(t, result, expected)
}

func Test7(t *testing.T) {
	result := bonetrousle(39, 15, 3)
	var expected [][]int64 = [][]int64{{10, 14, 15}}
	check(t, result, expected)
}

func Test8(t *testing.T) {
	result := bonetrousle(95, 20, 10)
	var expected [][]int64 = [][]int64{{17, 1, 2, 3, 4, 5, 6, 18, 19, 20}}
	check(t, result, expected)
}

func Test9(t *testing.T) {
	result := bonetrousle(38, 10, 7)
	var expected [][]int64 = [][]int64{{5, 1, 2, 3, 8, 9, 10}}
	check(t, result, expected)
}

func Test10(t *testing.T) {
	result := bonetrousle(2, 3, 1)
	var expected [][]int64 = [][]int64{{2}}
	check(t, result, expected)
}

func Test11(t *testing.T) {
	result := bonetrousle(125, 16, 14)
	var expected [][]int64 = [][]int64{{7, 1, 2, 3, 4, 8, 9, 10, 11, 12, 13, 14, 15, 16}}
	check(t, result, expected)
}

func Test12(t *testing.T) {
	result := bonetrousle(77, 18, 7)
	var expected [][]int64 = [][]int64{{8, 1, 2, 15, 16, 17, 18}}
	check(t, result, expected)
}

func Test13(t *testing.T) {
	result := bonetrousle(3, 3, 1)
	var expected [][]int64 = [][]int64{{3}}
	check(t, result, expected)
}

func Test14(t *testing.T) {
	result := bonetrousle(172, 17, 9)
	var expected []int64 = []int64{-1}
	if !equalSlices(result, expected) {
		t.Error("For", expected,
			"Got", result)
	}
}

func Test15(t *testing.T) {
	result := bonetrousle(124, 19, 11)
	var expected [][]int64 = [][]int64{{6, 1, 2, 3, 13, 14, 15, 16, 17, 18, 19}}
	check(t, result, expected)
}

func Test16(t *testing.T) {
	result := bonetrousle(3, 2, 2)
	var expected [][]int64 = [][]int64{{1, 2}}
	check(t, result, expected)
}

func Test17(t *testing.T) {
	result := bonetrousle(173, 19, 18)
	var expected [][]int64 = [][]int64{{18, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 19}}
	check(t, result, expected)
}

func Test18(t *testing.T) {
	result := bonetrousle(50, 16, 7)
	var expected [][]int64 = [][]int64{{9, 1, 2, 3, 4, 15, 16}}
	check(t, result, expected)
}

func Test19(t *testing.T) {
	result := bonetrousle(122, 16, 13)
	var expected [][]int64 = [][]int64{{4, 1, 2, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16}}
	check(t, result, expected)
}

func Test20(t *testing.T) {
	result := bonetrousle(73, 19, 6)
	var expected [][]int64 = [][]int64{{16, 1, 2, 17, 18, 19}}
	check(t, result, expected)
}

func Test21(t *testing.T) {
	result := bonetrousle(7, 14, 1)
	var expected [][]int64 = [][]int64{{7}}
	check(t, result, expected)
}

func Test22(t *testing.T) {
	result := bonetrousle(6, 3, 3)
	var expected [][]int64 = [][]int64{{3, 1, 2}}
	check(t, result, expected)
}

func Test23(t *testing.T) {
	result := bonetrousle(13, 17, 4)
	var expected [][]int64 = [][]int64{{7, 1, 2, 3}}
	check(t, result, expected)
}

func Test24(t *testing.T) {
	result := bonetrousle(59, 13, 7)
	var expected [][]int64 = [][]int64{{3, 1, 9, 10, 11, 12, 13}}
	check(t, result, expected)
}

func Test25(t *testing.T) {
	result := bonetrousle(31, 8, 7)
	var expected [][]int64 = [][]int64{{6, 1, 2, 3, 4, 7, 8}}
	check(t, result, expected)
}

func Test26(t *testing.T) {
	result := bonetrousle(11, 13, 3)
	var expected [][]int64 = [][]int64{{8, 1, 2}}
	check(t, result, expected)
}

func Test27(t *testing.T) {
	result := bonetrousle(3, 6, 2)
	var expected [][]int64 = [][]int64{{2, 1}}
	check(t, result, expected)
}

func Test28(t *testing.T) {
	result := bonetrousle(4, 7, 1)
	var expected [][]int64 = [][]int64{{4}}
	check(t, result, expected)
}

func Test29(t *testing.T) {
	result := bonetrousle(78, 34, 9)
	var expected [][]int64 = [][]int64{{16, 1, 2, 3, 4, 5, 6, 7, 34}}
	check(t, result, expected)
}

func Test30(t *testing.T) {
	result := bonetrousle(79, 30, 12)
	var expected [][]int64 = [][]int64{{13, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}}
	check(t, result, expected)
}

func Test31(t *testing.T) {
	result := bonetrousle(34, 82, 5)
	var expected [][]int64 = [][]int64{{24, 1, 2, 3, 4}}
	check(t, result, expected)
}

func Test32(t *testing.T) {
	result := bonetrousle(185, 231, 16)
	var expected [][]int64 = [][]int64{{65, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}}
	check(t, result, expected)
}

func Test33(t *testing.T) {
	result := bonetrousle(48, 84, 2)
	var expected [][]int64 = [][]int64{{47, 1}}
	check(t, result, expected)
}

func Test34(t *testing.T) {
	result := bonetrousle(232, 9, 3)
	var expected []int64 = []int64{-1}
	if !equalSlices(result, expected) {
		t.Error("For", expected,
			"Got", result)
	}
}

func Test35(t *testing.T) {
	result := bonetrousle(9, 5, 4)
	var expected []int64 = []int64{-1}
	if !equalSlices(result, expected) {
		t.Error("For", expected,
			"Got", result)
	}
}

func Test36(t *testing.T) {
	result := bonetrousle(234, 40, 16)
	var expected [][]int64 = [][]int64{{14, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 37, 38, 39, 40}}
	check(t, result, expected)
}

func Test37(t *testing.T) {
	result := bonetrousle(255, 39, 10)
	var expected [][]int64 = [][]int64{{30, 1, 2, 3, 34, 35, 36, 37, 38, 39}}
	check(t, result, expected)
}

func Test38(t *testing.T) {
	result := bonetrousle(60, 55, 4)
	var expected [][]int64 = [][]int64{{54, 1, 2, 3}}
	check(t, result, expected)
}

func Test39(t *testing.T) {
	result := bonetrousle(286, 164, 3)
	var expected [][]int64 = [][]int64{{121, 1, 164}}
	check(t, result, expected)
}

func Test40(t *testing.T) {
	result := bonetrousle(90, 14, 10)
	var expected [][]int64 = [][]int64{{5, 1, 7, 8, 9, 10, 11, 12, 13, 14}}
	check(t, result, expected)
}

func Test41(t *testing.T) {
	result := bonetrousle(280, 300, 2)
	var expected [][]int64 = [][]int64{{279, 1}}
	check(t, result, expected)
}

func Test42(t *testing.T) {
	result := bonetrousle(2, 27, 1)
	var expected [][]int64 = [][]int64{{2}}
	check(t, result, expected)
}

func Test43(t *testing.T) {
	result := bonetrousle(223, 21, 19)
	var expected [][]int64 = [][]int64{{4, 1, 2, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21}}
	check(t, result, expected)
}

func Test44(t *testing.T) {
	result := bonetrousle(77, 47, 6)
	var expected [][]int64 = [][]int64{{20, 1, 2, 3, 4, 47}}
	check(t, result, expected)
}

func Test45(t *testing.T) {
	result := bonetrousle(280, 300, 1)
	var expected [][]int64 = [][]int64{{280}}
	check(t, result, expected)
}

func Test46(t *testing.T) {
	result := bonetrousle(173, 45, 10)
	var expected [][]int64 = [][]int64{{20, 1, 2, 3, 4, 5, 6, 43, 44, 45}}
	check(t, result, expected)
}

//func Test47(t *testing.T) {
//	result := bonetrousle(61, 15, 5)
//	var expected [][]int64 = [][]int64{{7, 12, 13, 14, 15}}
//	check(t, result, expected)
//}

func Test47(t *testing.T) {
	cc(t, []int64{61, 15, 5}, [][]int64{{7, 12, 13, 14, 15}})
}

func TestTC5(t *testing.T) {
	cc(t, []int64{20, 10, 5}, [][]int64{{10, 1, 2, 3, 4}})
	cc(t, []int64{236, 22, 21}, [][]int64{{18, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22}})
	cc(t, []int64{3, 3, 2}, [][]int64{{2, 1}})
	cc(t, []int64{228, 36, 14}, [][]int64{{22, 1, 2, 3, 4, 5, 6, 7, 8, 32, 33, 34, 35, 36}})
	cc(t, []int64{235, 24, 19}, [][]int64{{16, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 17, 18, 19, 20, 21, 22, 23, 24}})
	cc(t, []int64{131, 75, 14}, [][]int64{{40, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13}})
	//// TODO: 176s
	////cc(t, []int64{151, 102, 67}, [][]int64{{-1}})
	cc(t, []int64{34, 14, 7}, [][]int64{{13, 1, 2, 3, 4, 5, 6}})
	cc(t, []int64{69, 12, 9}, [][]int64{{5, 1, 6, 7, 8, 9, 10, 11, 12}})
	cc(t, []int64{14, 10, 3}, [][]int64{{3, 1, 10}})
	cc(t, []int64{169, 29, 7}, [][]int64{{10, 24, 25, 26, 27, 28, 29}})
	cc(t, []int64{31, 9, 6}, [][]int64{{4, 1, 2, 7, 8, 9}})
	//// TODO: 250s
	////cc(t, []int64{245, 86, 18}, [][]int64{{23, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 86}})
	cc(t, []int64{14, 177, 19}, [][]int64{{-1}})
	cc(t, []int64{215, 262, 7}, [][]int64{{194, 1, 2, 3, 4, 5, 6}})
	cc(t, []int64{63, 14, 6}, [][]int64{{3, 10, 11, 12, 13, 14}})
	cc(t, []int64{124, 17, 15}, [][]int64{{16, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 17}})
	cc(t, []int64{151, 137, 2}, [][]int64{{14, 137}})
	cc(t, []int64{56, 12, 8}, [][]int64{{8, 1, 2, 3, 9, 10, 11, 12}})
	cc(t, []int64{248, 70, 16}, [][]int64{{18, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 69, 70}})
}
