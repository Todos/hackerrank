package main

import (
	"fmt"
)

// Complete the checkMagazine function below.
func checkMagazine(magazine []string, note []string) {
	if checkMagazineOK(magazine, note) {
		fmt.Println("Yes")
	} else {
		fmt.Println("No")
	}
}

func checkMagazineOK(magazine []string, note []string) bool {
	var result bool = true
	var magazMap = map[string]int{}
	for _, m := range magazine {
		count, ok := magazMap[m]
		if !ok {
			magazMap[m] = 1
		} else {
			magazMap[m] = count + 1
		}
	}
	for i := 0; i < len(note); i++ {
		count, ok := magazMap[note[i]]
		if !ok || count <= 0 {
			result = false
			break
		} else {
			count = count - 1
			magazMap[note[i]] = count
		}
	}
	return result
}
