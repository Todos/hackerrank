package main

import (
	"sort"
)

type Pair struct {
	Key   int32
	Value int32
}

// Complete the minimumSwaps function below.
func minimumSwaps(arr []int32) int32 {
	var pairs []Pair = []Pair{}
	for i := 0; i < len(arr); i++ {
		var pair Pair = Pair{arr[i], int32(i)}
		pairs = append(pairs, pair)
	}
	sort.Slice(pairs, func(i, j int) bool { return pairs[i].Key < pairs[j].Key })
	var visited []bool = []bool{}

	for i := 0; i < len(pairs); i++ {
		visited = append(visited, false)
	}

	var result int32 = 0
	for i := 0; i < len(pairs); i++ {
		if visited[i] == true || pairs[i].Value == int32(i) {
			continue
		}

		var cycleSize int32 = 0
		var j int = i
		for {
			if visited[j] {
				break
			}
			visited[j] = true
			j = int(pairs[j].Value)
			cycleSize++
		}
		result += (cycleSize - 1)

	}
	return result
}
