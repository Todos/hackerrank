package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func minimumBribes(q []int32) {
	result := minimumBribesResultInt(q)
	if result < 0 {
		fmt.Println("Too chaotic")
	} else {
		fmt.Println(result)
	}
}

func swapSliceElements(q *[]int32, i int, j int) {
	tmp := (*q)[i]
	(*q)[i] = (*q)[j]
	(*q)[j] = tmp
}

func minimumBribesResultInt(q []int32) int {
	// algorithm uses bubble sorting
	var swaps map[int32]int32 = map[int32]int32{}
	var swapCount int = 0
	var i int = 0
	for {
		if q[i] > q[i+1] {
			// check availability in swaps map
			element, ok := swaps[q[i]]
			if !ok {
				swaps[q[i]] = 1
			} else {
				swaps[q[i]] = element + 1
			}
			if swaps[q[i]] >= 3 {
				// early exit
				return -1
			}
			// swap the elements
			swapSliceElements(&q, i, i+1)
			// increment swap count
			swapCount++
			// reset the index
			i -= 3
			if i < 0 {
				i = 0
			}
			continue
		}
		// increment index
		i++
		if i >= len(q)-1 {
			break
		}
	}
	//fmt.Println(q, swaps, swapCount)
	return swapCount
}

//func main() {
//	q := []int32{2, 1, 3, 5, 4}
//	fmt.Println(minimumBribesResultInt(q))
//
//	q = []int32{3, 1, 2, 6, 5, 4}
//	fmt.Println(minimumBribesResultInt(q))
//
//	q = []int32{2, 1, 5, 3, 4}
//	fmt.Println(minimumBribesResultInt(q))
//
//	q = []int32{2, 5, 1, 3, 4}
//	fmt.Println(minimumBribesResultInt(q))
//}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	tTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)
	t := int32(tTemp)

	for tItr := 0; tItr < int(t); tItr++ {
		nTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
		checkError(err)
		n := int32(nTemp)

		qTemp := strings.Split(readLine(reader), " ")

		var q []int32

		for i := 0; i < int(n); i++ {
			qItemTemp, err := strconv.ParseInt(qTemp[i], 10, 64)
			checkError(err)
			qItem := int32(qItemTemp)
			q = append(q, qItem)
		}

		minimumBribes(q)
	}
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
