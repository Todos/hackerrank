package main

import (
	"testing"
)

func checkFlavors(t *testing.T, cost []int32, money int32, exp []int32) {
	result := whatFlavorsArr(cost, money)
	if (len(result) == 0 && len(exp) != 0) || (len(result) != len(exp)) {
		t.Error("For cost", cost,
			"money", money,
			"got", result,
			"expected", exp)
		return
	}
	for i := 0; i < len(exp); i++ {
		if result[i] != exp[i] {
			t.Error("For cost", cost,
				"money", money,
				"got", result,
				"expected", exp)
			break
		}
	}
	return
}

func checkSearch(t *testing.T, array []int32, x int32, exp int32) {
	result := searchNum(array, x, 0, int32(len(array)))
	if result != exp {
		t.Error("For", array,
			"value", x,
			"expected", exp,
			"got", result)
	}
}

func Test00(t *testing.T) {
	checkSearch(t, []int32{10, 20, 30, 40, 50}, 20, 1)
	checkSearch(t, []int32{10, 20, 30, 40}, 10, 0)
	checkSearch(t, []int32{10, 20, 30, 40}, 30, 2)
	checkSearch(t, []int32{10, 20, 30, 40}, 40, 3)
	checkSearch(t, []int32{10}, 10, 0)
	checkSearch(t, []int32{}, 10, -1)
	checkSearch(t, []int32{100}, 10, -1)
}

func Test01(t *testing.T) {
	checkFlavors(t, []int32{1, 4, 5, 3, 2}, 4, []int32{1, 4})
	checkFlavors(t, []int32{2, 2, 4, 3}, 4, []int32{1, 2})
	checkFlavors(t, []int32{3, 3, 1, 4}, 6, []int32{1, 2})
	checkFlavors(t, []int32{1, 4, 2}, 6, []int32{2, 3})
	checkFlavors(t, []int32{}, 0, []int32{})
	checkFlavors(t, []int32{1, 1, 2}, 2, []int32{1, 2})
	checkFlavors(t, []int32{10, 1, 3, 9}, 11, []int32{1, 2})
	checkFlavors(t, []int32{10}, 10, []int32{})
}
