package main

import (
	"testing"
)

func check(t *testing.T, arr []int32, exp int32) {
	var res int32 = minimumSwaps(arr)
	if res != exp {
		t.Error("For", arr,
			"should", exp,
			"got", res)
	}
}

func Test1(t *testing.T) {
	check(t, []int32{7, 1, 3, 2, 4, 5, 6}, 5)
	check(t, []int32{4, 3, 1, 2}, 3)
	check(t, []int32{2, 3, 4, 1, 5}, 3)
	check(t, []int32{1, 3, 5, 2, 4, 6, 8}, 3)
}
