package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
)

// Complete the whatFlavors function below.
func whatFlavors(cost []int32, money int32) {
	result := whatFlavorsArr(cost, money)
	fmt.Printf("%d %d\n", result[0], result[1])
}

func searchNum(array []int32, x int32, left int32, right int32) int32 {
	if len(array) == 0 {
		return -1
	}
	if left > right {
		return -1
	}
	mid := (left + right) / 2
	if array[mid] < x {
		return searchNum(array, x, mid+1, right)
	} else if array[mid] > x {
		return searchNum(array, x, left, mid-1)
	}
	return mid
}

func whatFlavorsArr(cost []int32, money int32) (result []int32) {
	// cost = [30, 50, 20, 10]
	// costPairs = [[30, 0], [50, 1], [20, 2], [10, 3]]
	// after sorting, costPairs = [[10, 3], [20, 2], [30, 0], [50, 1]]
	var costPairs [][]int32 = [][]int32{}
	for i := 0; i < len(cost); i++ {
		costPairs = append(costPairs, []int32{cost[i], int32(i)})
	}
	sort.Slice(costPairs, func(i, j int) bool { return costPairs[i][0] < costPairs[j][0] })

	// costSorted is a copy of costPairs
	var costSorted []int32 = []int32{}
	for i := 0; i < len(costPairs); i++ {
		costSorted = append(costSorted, costPairs[i][0])
	}
	var pair []int32
	for index, _ := range costSorted {
		idI := int32(index)

		i, costSortedTail := costSorted[index], costSorted[index+1:]
		j := money - i

		idJ := searchNum(costSortedTail, j, 0, int32(len(costSortedTail))-1)
		// fix coordinate
		if idJ < 0 {
			continue
		}
		idJ = idI + idJ + 1
		pair = []int32{idI, idJ}
		break
	}
	if len(pair) == 0 {
		return
	}
	for i := 0; i < len(costPairs); i++ {
		if int32(i) == pair[0] || int32(i) == pair[1] {
			result = append(result, costPairs[i][1]+1)
		}
	}
	sort.Slice(result, func(i, j int) bool { return result[i] < result[j] })
	return
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	tTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)
	t := int32(tTemp)

	for tItr := 0; tItr < int(t); tItr++ {
		moneyTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
		checkError(err)
		money := int32(moneyTemp)

		nTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
		checkError(err)
		n := int32(nTemp)

		costTemp := strings.Split(readLine(reader), " ")

		var cost []int32

		for i := 0; i < int(n); i++ {
			costItemTemp, err := strconv.ParseInt(costTemp[i], 10, 64)
			checkError(err)
			costItem := int32(costItemTemp)
			cost = append(cost, costItem)
		}

		whatFlavors(cost, money)
	}
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
