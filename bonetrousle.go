package main

import (
	"bufio"
	"fmt"
	"io"
	//"math/rand"
	"os"
	"strconv"
	"strings"
	//"time"
)

func bonetrousle(n int64, k int64, b int32) []int64 {
	var arrK []int64 = []int64{}
	var i int64 = 1
	for ; i <= k; i++ {
		if i <= n {
			// Not adding already bigger
			arrK = append(arrK, i)
		}
	}
	result := subsetSum(arrK, n, b)
	if len(result) > 0 {
		return result[0]
	}
	return []int64{-1}
}

func subsetSum(arrK []int64, paramN int64, paramB int32) [][]int64 {
	var result [][]int64 = [][]int64{}
	var subsetFunc func(arry []int64, partial []int64)
	subsetFunc = func(arry []int64, partial []int64) {
		var s int64 = 0
		for _, x := range partial {
			s += x
		}
		// check if the partial sum equals to N
		if s == paramN && int32(len(partial)) == paramB {
			var tmparr []int64 = []int64{}
			for k := 0; k < len(partial); k++ {
				tmparr = append(tmparr, partial[k])
			}
			result = append(result, tmparr)
		}
		// if we reach the number why bother to continue
		if s >= paramN {
			return
		}
		if int32(len(partial)) > paramB {
			return
		}
		// rand:
		//rand.Seed(time.Now().UTC().UnixNano())
		//for k, _ := range arry {
		//	l := rand.Intn(k + 1)
		//	arry[k], arry[l] = arry[l], arry[k]
		//}

		var maxind int = -1
		var maxelm int64 = -1
		for i, _ := range arry {
			if arry[i] > maxelm {
				maxelm = arry[i]
				maxind = i
			}
		}
		if maxind >= 0 {
			arry = append(arry[:maxind], arry[maxind+1:]...)

			//rand.Seed(time.Now().UTC().UnixNano())
			//for k, _ := range arry {
			//	l := rand.Intn(k + 1)
			//	arry[k], arry[l] = arry[l], arry[k]
			//}

			arry = append(arry, 0)
			copy(arry[1:], arry[0:])
			arry[0] = maxelm
		}
		//fmt.Println("arry", arry)

		for i := 0; i < len(arry); i++ {
			var remaining []int64 = arry[i+1:]
			tmp := append(partial, arry[i])
			subsetFunc(remaining, tmp)
			if len(result) >= 1 {
				// if we reach the result get out of this recursion
				return
			}
		}
	}
	var partial []int64 = []int64{}
	subsetFunc(arrK, partial)

	return result
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	stdout, err := os.Create(os.Getenv("OUTPUT_PATH"))
	checkError(err)

	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 1024*1024)

	tTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)
	t := int32(tTemp)

	for tItr := 0; tItr < int(t); tItr++ {
		nkb := strings.Split(readLine(reader), " ")

		n, err := strconv.ParseInt(nkb[0], 10, 64)
		checkError(err)

		k, err := strconv.ParseInt(nkb[1], 10, 64)
		checkError(err)

		bTemp, err := strconv.ParseInt(nkb[2], 10, 64)
		checkError(err)
		b := int32(bTemp)

		result := bonetrousle(n, k, b)

		for resultItr, resultItem := range result {
			fmt.Fprintf(writer, "%d", resultItem)

			if resultItr != len(result)-1 {
				fmt.Fprintf(writer, " ")
			}
		}

		fmt.Fprintf(writer, "\n")
	}

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
