package main

import (
	"testing"
)

func testCalcPath(t *testing.T, n int, m int, uvs [][]int, s int, exp []int) {
	res := calcPath(n, m, uvs, s)
	if len(res) != len(exp) {
		t.Error("For", n, m, uvs, s,
			"expected", exp,
			"got", res)
		return
	}
	for i := 0; i < len(exp); i++ {
		if exp[i] != res[i] {
			t.Error("For", n, m, uvs, s,
				"expected", exp,
				"got", res)
			break
		}
	}

}

func Test0(t *testing.T) {
	testCalcPath(t, 4, 2, [][]int{{1, 2}, {1, 3}}, 1, []int{6, 6, -1})
	testCalcPath(t, 3, 1, [][]int{{2, 3}}, 2, []int{-1, 6})
}

func Test1(t *testing.T) {
	testCalcPath(t, 6, 4, [][]int{{1, 2}, {2, 3}, {3, 4}, {1, 5}}, 1, []int{6, 12, 18, 6, -1})
}

func Test2(t *testing.T) {
	testCalcPath(t, 7, 4, [][]int{{1, 2}, {1, 3}, {3, 4}, {2, 5}}, 2, []int{6, 12, 18, 6, -1, -1})
}

//func Test3(t *testing.T) {
//	testCalcPath(t, 4, 6, [][]int{{1, 2}, {1, 3}, {2, 3}, {3, 1}, {3, 4}, {4, 4}}, 3, []int{6, 12, 18, 24})
//}
