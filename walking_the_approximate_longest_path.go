package main

/*
The following algorithm works:

- Start with any city with fewest neighbors
- Proceed to that city's neighbor that has the fewest neighbors (but at least 2, to avoid a spur)
- Remove roads as you traverse them
- The last city will be a spur, of course
*/

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func parseInput() (result [][]int) {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line := scanner.Text()
		splits := strings.Fields(line)
		if len(splits) > 0 {
			x, _ := strconv.ParseInt(splits[0], 10, 0)
			y, _ := strconv.ParseInt(splits[1], 10, 0)
			result = append(result, []int{int(x), int(y)})
		}
	}
	return
}

func appendByKeyIfNotPresent(arr *map[int][]int, key int, val int) {
	values, ok := (*arr)[key]
	if ok {
		isVal := false
		for _, i := range values {
			if val == i {
				isVal = true
				break
			}
		}
		if !isVal {
			(*arr)[key] = append((*arr)[key], val)
		}
	} else {
		(*arr)[key] = []int{val}
	}
}

func buildGraph(edges [][]int) (result map[int][]int) {
	result = map[int][]int{}
	for i := 0; i < len(edges); i++ {
		x, y := edges[i][0], edges[i][1]
		appendByKeyIfNotPresent(&result, x, y)
		appendByKeyIfNotPresent(&result, y, x)
	}
	return
}

func nodeWithMinEdges(graph map[int][]int, excludeNode int) (result int) {
	var minEdges int = 1000000
	for node, edges := range graph {
		if len(edges) < minEdges && node != excludeNode {
			minEdges = len(edges)
			result = node
		}
	}

	return
}

func nodeWithMinEdgesFromNodes(graph map[int][]int, nodes []int) (result int) {
	var minEdges int = 1000000
	for _, node := range nodes {
		if len(graph[node]) < minEdges {
			minEdges = len(graph[node])
			result = node
		}
	}

	return
}

func removeNode(arr []int, node int) (result []int) {
	var idx int = -1
	for i := 0; i < len(arr); i++ {
		if arr[i] == node {
			idx = i
			break
		}
	}
	if idx < 0 {
		// not found
		result = arr[:]
	} else {
		result = append(arr[:idx], arr[idx+1:]...)
	}
	return
}

func findPath(graph map[int][]int, node int) (result []int) {
	for len(graph[node]) != 0 {
		nodes := graph[node]
		nextNode := nodeWithMinEdgesFromNodes(graph, nodes)

		result = append(result, node)

		graph[node] = removeNode(graph[node], nextNode)
		graph[nextNode] = removeNode(graph[nextNode], node)

		node = nextNode
	}
	return
}

func findPathFromEdges(edges [][]int) (result []int) {
	graph := buildGraph(edges)
	startNode := nodeWithMinEdges(graph, 0)
	//startNode := 1
	result = findPath(graph, startNode)
	return
}

func main() {
	inputArr := parseInput()

	// the number of cities, roads
	//n, m := inputArr[0][0], inputArr[0][1]
	// the edges
	var edges [][]int = inputArr[1:]

	path := findPathFromEdges(edges)
	fmt.Println(len(path))
	fmt.Println(strings.Trim(strings.Join(strings.Split(fmt.Sprint(path), " "), " "), "[]"))
}
