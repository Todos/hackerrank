package main

import (
	"bufio"
	"container/list"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Rec struct {
	Left  int
	Right int
	Count int
}

func parseInput() (result []Rec) {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line := scanner.Text()
		splits := strings.Fields(line)
		rec := Rec{}
		if len(splits) == 1 {
			x, _ := strconv.ParseInt(splits[0], 10, 0)
			rec.Left = int(x)
			rec.Count = 1
		} else if len(splits) == 2 {
			x, _ := strconv.ParseInt(splits[0], 10, 0)
			y, _ := strconv.ParseInt(splits[1], 10, 0)
			rec.Left = int(x)
			rec.Right = int(y)
			rec.Count = 2
		}

		if rec.Count > 0 {
			result = append(result, rec)
		}
	}
	return
}

func parseData(recs []Rec) (result [][]interface{}) {
	index := 0
	q := int(recs[index].Left)
	index++

	for i := 0; i < q; i++ {
		n := recs[index].Left
		m := recs[index].Right

		uvs := [][]int{}
		for j := 0; j < m; j++ {
			index++
			u := recs[index].Left
			v := recs[index].Right
			uvs = append(uvs, []int{u, v})
		}
		index++
		s := recs[index].Left

		result = append(result, []interface{}{n, m, uvs, s})
		index++
	}
	return
}

func calcPath(n int, m int, uvs [][]int, s int) (result []int) {
	//fmt.Printf("n:%d, m:%d, uvs:%v, s:%d\n", n, m, uvs, s)
	graph := new(Graph)
	graph.Init(n)
	for i := 0; i < m; i++ {
		// -1 to begin from 0
		graph.AddEdge(uvs[i][0]-1, uvs[i][1]-1)
		graph.AddEdge(uvs[i][1]-1, uvs[i][0]-1)
	}
	//fmt.Printf("graph:%#v\n", graph)
	result = graph.BFS(s - 1)
	return
}

// Graph class
type Graph struct {
	V   int
	Adj [][]int
}

func (g *Graph) Init(v int) {
	g.V = v
	g.Adj = [][]int{}
	for i := 0; i < v; i++ {
		g.Adj = append(g.Adj, []int{})
	}
}

func (g *Graph) AddEdge(u int, v int) {
	g.Adj[u] = append(g.Adj[u], v)
}

func (g *Graph) BFS(s int) (result []int) {
	visited := []bool{}
	distances := []int{}
	for i := 0; i < g.V; i++ {
		visited = append(visited, false)
		distances = append(distances, -1)
	}

	queue := list.New()
	queue.PushBack(s)
	visited[s] = true
	distances[s] = 0
	startS := s

	for queue.Len() > 0 {
		elem := queue.Front()
		s = elem.Value.(int)
		queue.Remove(elem)

		for i := 0; i < len(g.Adj[s]); i++ {
			j := g.Adj[s][i]
			if !visited[j] {
				visited[j] = true
				distances[j] = distances[s] + 6
				queue.PushBack(j)
			}
		}
	}

	for i := 0; i < len(distances); i++ {
		if i != startS {
			result = append(result, distances[i])
		}
	}

	return
}

func main() {
	input := parseInput()
	//fmt.Println(input)
	data := parseData(input)
	//fmt.Println(data)

	for _, q := range data {
		n, _ := q[0].(int)
		m, _ := q[1].(int)
		uvs := q[2].([][]int)
		s, _ := q[3].(int)

		res := calcPath(n, m, uvs, s)
		for i := 0; i < len(res); i++ {
			fmt.Printf("%d", res[i])
			if i == len(res)-1 {
				fmt.Printf("\n")
			} else {
				fmt.Printf(" ")
			}
		}
	}
}
