package main

import (
	"testing"
)

func check(t *testing.T, magazine []string, note []string, exp bool) {
	res := checkMagazineOK(magazine, note)
	if res != exp {
		t.Error("Magazine", magazine,
			"note", note,
			"expected", exp,
			"got", res)
	}
}

func Test1(t *testing.T) {
	check(t, []string{"give", "me", "one", "grand", "today", "night"},
		[]string{"give", "one", "grand", "today"}, true)

	check(t, []string{"two", "times", "three", "is", "not", "four"},
		[]string{"two", "times", "two", "is", "four"}, false)

	check(t, []string{"ive", "got", "a", "lovely", "bunch", "of", "coconuts"},
		[]string{"ive", "got", "some", "coconuts"}, false)
}
