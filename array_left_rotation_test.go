package main

import (
	"testing"
)

func check(t *testing.T, inArr []int32, d int32, expArr []int32) {
	resArr := rotLeft(inArr, d)
	for i := 0; i < len(resArr); i++ {
		if resArr[i] != expArr[i] {
			t.Error("resArr", resArr, "expArr", expArr)
			break
		}
	}

}

func Test1(t *testing.T) {
	check(t,
		[]int32{1, 2, 3, 4, 5}, 3,
		[]int32{4, 5, 1, 2, 3})
}

func Test2(t *testing.T) {
	check(t,
		[]int32{1, 2, 3, 4, 5}, 1,
		[]int32{2, 3, 4, 5, 1})
}

func Test3(t *testing.T) {
	check(t,
		[]int32{1, 2}, 1,
		[]int32{2, 1})
}

func Test4(t *testing.T) {
	check(t,
		[]int32{1}, 1,
		[]int32{1})
}

func Test5(t *testing.T) {
	check(t,
		[]int32{}, 1,
		[]int32{})
}

func Test6(t *testing.T) {
	check(t,
		[]int32{1, 2}, -1,
		[]int32{1, 2})
}
