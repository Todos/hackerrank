package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type Node struct {
	Val  int32
	Prev *Node
	Next *Node
}

// Complete the rotLeft function below.
func rotLeft(a []int32, d int32) []int32 {
	var res []int32 = []int32{}
	if len(a) == 0 {
		return res
	}

	var node *Node = &Node{a[0], nil, nil}
	var head *Node = node
	for i := 1; i < len(a); i++ {
		var nextNode *Node = &Node{a[i], nil, nil}
		nextNode.Prev = node
		node.Next = nextNode
		node = nextNode
	}
	var tail *Node = node

	for {
		if d <= 0 {
			break
		}

		var tmpHead Node = *head
		if tmpHead.Next == nil {
			break
		}
		head = tmpHead.Next
		tmpHead.Next.Prev = nil

		tmpHead.Prev = tail
		tmpHead.Next = nil
		tail.Next = &tmpHead
		tail = &tmpHead

		d--
	}

	//fmt.Printf("input: %#v\n", a)
	var n1 *Node = head
	for {
		//fmt.Printf("%p %#v\n", n1, n1)
		res = append(res, n1.Val)
		n1 = n1.Next
		if n1 == nil {
			break
		}
	}
	//fmt.Printf("head: %#v\n", head)
	//fmt.Printf("tail: %#v\n", tail)

	return res
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	stdout, err := os.Create(os.Getenv("OUTPUT_PATH"))
	checkError(err)

	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 1024*1024)

	nd := strings.Split(readLine(reader), " ")

	nTemp, err := strconv.ParseInt(nd[0], 10, 64)
	checkError(err)
	n := int32(nTemp)

	dTemp, err := strconv.ParseInt(nd[1], 10, 64)
	checkError(err)
	d := int32(dTemp)

	aTemp := strings.Split(readLine(reader), " ")

	var a []int32

	for i := 0; i < int(n); i++ {
		aItemTemp, err := strconv.ParseInt(aTemp[i], 10, 64)
		checkError(err)
		aItem := int32(aItemTemp)
		a = append(a, aItem)
	}

	result := rotLeft(a, d)

	for i, resultItem := range result {
		fmt.Fprintf(writer, "%d", resultItem)

		if i != len(result)-1 {
			fmt.Fprintf(writer, " ")
		}
	}

	fmt.Fprintf(writer, "\n")

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
