package main

import (
	"testing"
)

func Test11(t *testing.T) {
	var map1 map[int][]int = map[int][]int{
		1: []int{12, 13},
		2: []int{3},
		3: []int{4, 5},
		5: []int{6, 7, 9},
	}
	exp := 2
	result := nodeWithMinEdges(map1, 0)
	if result != exp {
		t.Error("map1", map1,
			"expected", exp,
			"got", result)
	}
}

func Test21(t *testing.T) {
	var map1 map[int][]int = map[int][]int{
		1: []int{12, 13},
		2: []int{3},
		3: []int{4, 5},
		5: []int{6, 7, 9},
	}
	var arr1 []int = []int{2, 5}
	exp := 2
	result := nodeWithMinEdgesFromNodes(map1, arr1)
	if result != exp {
		t.Error("map1", map1,
			"arr1", arr1,
			"expected", exp,
			"got", result)
	}
}

func checkRemove(t *testing.T, arr []int, elt int, exp []int) {
	result := removeNode(arr, elt)
	if len(result) != len(exp) {
		t.Error("arr", arr,
			"elt", elt,
			"exp", exp,
			"length differ")
		return
	}

	for i := 0; i < len(exp); i++ {
		if result[i] != exp[i] {
			t.Error("arr", arr,
				"elt", elt,
				"exp", exp,
				"i-th element differ", i,
				result[i], exp[i])
		}
	}
}

func Test31(t *testing.T) {
	var arr []int = []int{10, 9, 11, 15}
	var elt int = 10
	var exp []int = []int{9, 11, 15}
	checkRemove(t, arr, elt, exp)
}

func Test32(t *testing.T) {
	var arr []int = []int{}
	var elt int = 10
	var exp []int = []int{}
	checkRemove(t, arr, elt, exp)
}

func Test33(t *testing.T) {
	var arr []int = []int{2, 3}
	var elt int = 10
	var exp []int = []int{2, 3}
	checkRemove(t, arr, elt, exp)
}

func Test41(t *testing.T) {
	var edges [][]int = [][]int{
		[]int{3, 1},
		[]int{3, 4},
		[]int{2, 4},
		[]int{2, 3},
		[]int{4, 1},
	}
	var exp []int = []int{1, 3, 2, 4}
	result := findPathFromEdges(edges)
	if len(result) != len(exp) {
		t.Error("edges", edges,
			"exp", exp,
			"result", result,
			"length differ")
		return
	}

	for i := 0; i < len(exp); i++ {
		if result[i] != exp[i] {
			t.Error("edges", edges,
				"exp", exp,
				"result", result,
				"i-th element differ", i,
				result[i], exp[i])
		}
	}
}
